$pdf_mode = 1;        # tex -> pdf
$bibtex_use = 1;

@default_files = ('main.tex');

$clean_ext = "ps bbl nav out snm pdf synctex.gz";
